//
//  AnalyzeViewController.swift
//  face
//
//  Created by Admin on 15.02.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

import Foundation
import GoogleMobileAds
class AnalyzeViewController: GAITrackedViewController {
    
    @IBOutlet weak var resultImage: UIImageView!
    
    @IBOutlet weak var tipLabel: UILabel!
    var image: UIImage!
    var progress = UIView()
    
    @IBOutlet weak var bannerView: GADBannerView!

    

    override func viewDidLoad() {
        
        if (image != nil){
            
            let newSize = resultImage.frame.size
            let ratio = image.size.height / image.size.width;
            
            let rect = CGRectMake(0, 0, newSize.width, newSize.width * ratio)
            UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
            UIRectClip(rect)
            image.drawInRect(rect)
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            resultImage.image = newImage
            
        }
        
        
        
        var dict: NSDictionary?
        if let path = NSBundle.mainBundle().pathForResource("Strings", ofType: "plist"){
            dict = NSDictionary(contentsOfFile: path)
        } else {
            dict = NSDictionary()
        }
        
        bannerView.adUnitID = Settings.getSettings()["AdMob-banner"] as! String
        bannerView.rootViewController = self
        let request = GADRequest()
        //request.testDevices = [ "109352c33f88adb6447f0867b910bcf1"]
        bannerView.loadRequest(request)
        
        let tips = dict?["tips"] as! NSArray
        
        let randomIndex = Int(arc4random_uniform(UInt32(tips.count)))
        let tip = tips[randomIndex] as! NSString
        
        tipLabel.text = tip as String
        
        
        progress.layer.borderWidth = 2
        progress.layer.borderColor = UIColor.whiteColor().CGColor
        
        progress.layer.shadowColor = UIColor.blueColor().CGColor
        progress.layer.shadowOpacity = 1
        progress.layer.shadowOffset = CGSizeZero
        progress.layer.shadowRadius = 5
        
        progress.frame = CGRect(x: 0, y: 0, width: resultImage.bounds.width, height: 2)
        resultImage.addSubview(progress)
    }
    
    override func viewDidAppear(animated: Bool) {
        self.screenName = "Analyze screen"
        super.viewDidAppear(animated)
        UIView.animateWithDuration(Double(2.0), delay:Double(0.1), options: nil, animations: {
            self.progress.frame = CGRect(x: 0, y: self.resultImage.bounds.height, width: self.resultImage.bounds.width, height: 2)
            }, completion: { finished in
                self.performSegueWithIdentifier("resultsSegue", sender: self)
        })

    }
    
    
}