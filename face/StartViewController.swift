//
//  StartViewController.swift
//  face
//
//  Created by Admin on 25.01.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

import UIKit
import Social
import GoogleMobileAds
import TwitterKit

class StartViewController: GAITrackedViewController, GADInterstitialDelegate, VKSdkDelegate, FBLoginViewDelegate {
    
    
    
    var interstitial: GADInterstitial?
    
    var showAds = false
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var fbShareButton: UIButton!
    
    var texts:NSDictionary!
    
    override func viewDidAppear(animated: Bool) {
        self.screenName = "Start screen"
        super.viewDidAppear(animated)
    }
    
    func getStrings() -> NSDictionary {
        var dict: NSDictionary?
        if let path = NSBundle.mainBundle().pathForResource("Strings", ofType: "plist"){
            dict = NSDictionary(contentsOfFile: path)
        } else {
            dict = NSDictionary()
        }
        
        return dict!
    }
    @IBAction func moreTests(sender: AnyObject) {
        
        
        UIApplication.sharedApplication().openURL(NSURL(string: Settings.getSettings()["iTunes-URL-MoreApps"] as! String)!)
    }
    
    @IBAction func shareTW(sender: AnyObject) {
        
        let tracker = GAI.sharedInstance().defaultTracker
        var build = GAIDictionaryBuilder.createEventWithCategory("ui_action", action: "button", label: "start_screen_share_tw", value: nil).build() as [NSObject : AnyObject]
        tracker.send(build)
        
        var title : NSString = Settings.getSettings()["twitterCardURL"] as! NSString
        var str : NSString = (self.texts["sharing"] as! NSDictionary)["pre_scan"] as! NSString
        let strWithLink = (str as String)+" "+(title as String)
        
        
        if (UIApplication.sharedApplication().canOpenURL(NSURL(string: "twitter://")!))
        {
            var shareURLText : String = "twitter://post?message="+strWithLink
            shareURLText=shareURLText.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
            UIApplication.sharedApplication().openURL(NSURL(string: shareURLText)!)
            
        }
        else
        {
            Twitter.sharedInstance().logInWithCompletion { (session: TWTRSession!, error: NSError!) -> Void in
                if session != nil {
                    // Use the TwitterKit to create a Tweet composer.
                    let composer = TWTRComposer()
                    composer.setText(strWithLink)
                    // Present the composer to the user.
                    composer.showWithCompletion(nil)
                }
            }
        }
        
    }
    
    
    
    ////////////////////////////
    // Step 1. Login with permissions.
    ////////////////////////////
    func loginThenPost() -> Void{
        var permissions : NSArray = ["publish_actions","user_photos"]
        
        FBSession.openActiveSessionWithPublishPermissions(permissions as [AnyObject], defaultAudience: FBSessionDefaultAudience.Friends, allowLoginUI: true, completionHandler: {(session, state, error) -> Void in
            println(error)
            
            if(error != nil) {
                var alert : UIAlertView = UIAlertView(title: "Error", message: "Problem connecting with Facebook", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            } else {
                self.postPhoto()
            }
            
            
        })
        
    }
    
    ////////////////////////////
    // Step 2. Post that photo to FB.
    ////////////////////////////
    
    func postPhoto() -> Void {
        
        
        
        //        // We're going to assume you have a UIImage named image_ stored somewhere.
        //
        //        var connection : FBRequestConnection = FBRequestConnection()
        //
        //        // First request uploads the photo.
        //        var fbImage = UIImage(named: "cat1")
        //        var requestUpload : FBRequest = FBRequest(forUploadPhoto: fbImage)
        //        connection.addRequest(requestUpload,
        //            completionHandler:
        //            {
        //                (connection: FBRequestConnection!, result: AnyObject!, error: NSError!) -> Void in
        //                println(result)
        //                println(error)
        //                } as FBRequestHandler,
        //            batchEntryName: "photopost")
        //
        //
        //        // Second request retrieves photo information for just-created
        //        // photo so we can grab its source.
        //
        //        var requestGet : FBRequest = FBRequest(forGraphPath: "{result=photopost:$.id}")
        //
        //        connection.addRequest(requestGet,
        //            completionHandler:
        //            {
        //                (connection: FBRequestConnection!, result: AnyObject!, error: NSError!) -> Void in
        //                println(result)
        //                println(error)
        //                if (error == nil && result != nil) {
        //
        //                    if let jsonResult = result as? Dictionary<String, AnyObject> {
        //                        var ID_str : NSString = jsonResult["id"] as NSString
        //                        println(ID_str)
        //                        self.postDataWithPhoto(ID_str)
        //                        println("photo uploaded, lets post it to feed")
        //                    }
        //
        //                }
        //                } as FBRequestHandler)
        //
        //        connection.start()
    }
    
    @IBAction func shareFB(sender: AnyObject) {
        
        let tracker = GAI.sharedInstance().defaultTracker
        if (NSLocale.currentLocale().localeIdentifier == "ru_RU"){
            var build = GAIDictionaryBuilder.createEventWithCategory("ui_action", action: "button", label: "start_screen_share_vk", value: nil).build() as [NSObject : AnyObject]
        tracker.send( build)
            
            VKSdk.initializeWithDelegate(self, andAppId: Settings.getSettings()["VK-app-id"] as! String)
            var controller = VKShareDialogController()
            controller.text = (texts["sharing"] as! NSDictionary)["pre_scan"] as! NSString as String
            controller.completionHandler = { result in
                self.dismissViewControllerAnimated(true, completion: nil)
            }
            self.presentViewController(controller, animated: true, completion: nil)

            
        } else {
            var build = GAIDictionaryBuilder.createEventWithCategory("ui_action", action: "button", label: "start_screen_share_fb", value: nil).build() as [NSObject : AnyObject]
            tracker.send(build)
            
            
            var urlToShare : NSURL = NSURL(string: Settings.getSettings()["iTunes-URL"] as! String)!
            var title = texts?["title"] as! String
            var textToShare = (texts["sharing"] as! NSDictionary)["pre_scan"] as! String
            var urlToPic : NSURL =  NSURL(string: Settings.getSettings()["sharePic"] as! String)!
            
            // If it is available, we will first try to post using the share dialog in the Facebook app
            var params_ : FBLinkShareParams  =  FBLinkShareParams(link: urlToShare,
                name: title,
                caption: textToShare,
                description: textToShare,
                picture: urlToPic)
            
          
            
            //сначала приложение
            if  FBDialogs.canPresentShareDialogWithParams(params_)
            {
                FBDialogs.presentShareDialogWithParams(params_,
                    clientState: nil,  handler: {
                        call, results, error in
                        if (error != nil){
                            println(error)
                        }
                        else{
                            println("posted to FB")
                        }
                })
            }
            else {//нет приложения fb
                
                // Initialize variables
                
                let strToShare = textToShare + " " + (Settings.getSettings()["sharePic"] as! String)
                
                if (SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook)){
                    var controller = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
                    controller.setInitialText(strToShare)
                    controller.addURL(urlToShare)
                    self.presentViewController(controller, animated: true, completion: nil)
                }
                else{
                    
                    
                    // The user doesn't have the Facebook for iOS app installed.  You
                    // may be able to use a fallback.
                    
                    let link = Settings.getSettings()["iTunes-URL"] as! String
                    let description = (texts["sharing"] as! NSDictionary)["pre_scan"] as! NSString
                    var fbImage = Settings.getSettings()["sharePic"] as! String
                    let caption = description
                    
                    // Add variables to dictionary
                    var dict = NSMutableDictionary()
                    dict.setValue(title, forKey: "name")
                    dict.setValue(caption, forKey: "caption")
                    dict.setValue(description, forKey: "description")
                    dict.setValue(link, forKey: "link")
                    dict.setValue(fbImage, forKey: "picture")
                    
                    // Present the feed dialog and post the story
                    FBWebDialogs.presentFeedDialogModallyWithSession(nil, parameters: dict as [NSObject : AnyObject],  handler: {
                        call, results, error in
                        if (error != nil){
                            println(error)
                        }
                        else{
                            println("posted to FB")
                        }
                    })
                }
                
            }
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        texts = getStrings()
        
        var fbImage = UIImage(named: "facebook")
        
        //println(NSLocale.currentLocale().localeIdentifier)
        
        if (NSLocale.currentLocale().localeIdentifier == "ru_RU"){
            fbImage = UIImage(named: "vkontakte")
        }
        
        
        fbShareButton.setImage(fbImage, forState: .Normal)
        
        interstitial = GADInterstitial()
        interstitial?.adUnitID = Settings.getSettings()["AdMob-interstitial-start"] as! String
        interstitial?.delegate = self
        let request = GADRequest()
        request.testDevices = [ "109352c33f88adb6447f0867b910bcf1" ]
        
        interstitial?.loadRequest(request)
        
        bannerView.adUnitID = Settings.getSettings()["AdMob-banner"] as! String
        bannerView.rootViewController = self
        let bannerRequest = GADRequest()
        //bannerRequest.testDevices = [ "109352c33f88adb6447f0867b910bcf1" ]
        bannerView.loadRequest(bannerRequest)
        
        gaSignin()
        // Do any additional setup after loading the view.
    }
    
    func gaSignin(){
        let tracker = GAI.sharedInstance().defaultTracker
        
        tracker.set("&uid", value: "123")
        
        tracker.set(GAIFields.customDimensionForIndex(1), value: "123")
        
        var build = GAIDictionaryBuilder.createEventWithCategory("UX", action: "user signed in", label: nil, value: nil).build() as [NSObject : AnyObject]
        tracker.send(build)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func interstitialDidReceiveAd(ad: GADInterstitial!) {
        if (showAds){
            interstitial?.presentFromRootViewController(self)
        }
    }
    
    func vkSdkNeedCaptchaEnter(captchaError: VKError!) {
    }
    
    func vkSdkTokenHasExpired(expiredToken: VKAccessToken!) {
        
    }
    
    func vkSdkUserDeniedAccess(authorizationError: VKError!) {
    }
    
    func vkSdkShouldPresentViewController(controller: UIViewController!) {
    }
    
    func vkSdkReceivedNewToken(newToken: VKAccessToken!) {
    }
    
    func vkSdkIsBasicAuthorization() -> Bool {
        return false
    }
    
}
