//
//  ResultsViewController.swift
//  face
//
//  Created by Admin on 13.01.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

import UIKit
import Social
import GoogleMobileAds
import TwitterKit

protocol ResultsViewControllerDelegate{
    func resultsClosed(controller: ResultsViewController)
}

class ResultsViewController: GAITrackedViewController, UIDocumentInteractionControllerDelegate, VKSdkDelegate{
    
    var delegate: ResultsViewControllerDelegate? = nil
    let instagram = FInstagram()
    let locale = NSLocale.currentLocale()
    
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var bgView: UIImageView!
    
    @IBOutlet weak var fbShareButton: UIButton!
    
    var texts:NSDictionary?
    
    func getStrings() -> NSDictionary {
        var dict: NSDictionary?
        if let path = NSBundle.mainBundle().pathForResource("Strings", ofType: "plist"){
            dict = NSDictionary(contentsOfFile: path)
        } else {
            dict = NSDictionary()
        }
        
        return dict!
    }
    
    override func viewDidAppear(animated: Bool) {
        self.screenName = "Results screen"
        super.viewDidAppear(animated)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        texts = getStrings()
        let options = texts?["options"] as! NSArray
        
        var fbImage = UIImage(named: "facebook")
        
        //println(NSLocale.currentLocale().localeIdentifier)
        
        if (NSLocale.currentLocale().localeIdentifier == "ru_RU"){
            fbImage = UIImage(named: "vkontakte")
            VKSdk.initializeWithDelegate(self, andAppId: Settings.getSettings()["VK-app-id"] as! String)
            
            
        }
        
        fbShareButton.setImage(fbImage, forState: .Normal)
        
        bannerView.adUnitID = Settings.getSettings()["AdMob-banner"] as! String
        bannerView.rootViewController = self
        let request = GADRequest()
        //request.testDevices = [ "109352c33f88adb6447f0867b910bcf1" ]
        bannerView.loadRequest(request)
        
        //        bgView.image = UIImage(named: options[randomIndex] + "_blur")
        var checkImage = mainImageView.image as UIImage!
        var catFileName = ""
        var catName = ""
        while (checkImage == nil){
            var randomIndex = Int(arc4random_uniform(UInt32(options.count)))
            
            catFileName = String(format: "cat%02d", randomIndex+1)
            catName = options[randomIndex] as! String
            //println(catFileName)
            checkImage = UIImage (named: catFileName)
            
        }
        //var originalWidth  = checkImage.size.width
        
        mainImageView.image=textToImage(catName, inImage: checkImage!)
        // Do any additional setup after loading the view.
    }
    
    func textToImage(drawText: NSString, inImage: UIImage)->UIImage{
        
        var textColor: UIColor = UIColor.whiteColor()
        var textFont: UIFont = UIFont(name: "Helvetica Bold", size: 30)!
        
        UIGraphicsBeginImageContext(inImage.size)
        
        let textFontAttributes = [
            NSFontAttributeName: textFont,
            NSForegroundColorAttributeName: textColor,
        ]
        
        //Cat picture
        inImage.drawInRect(CGRectMake(20, 20, 460, 460))
        
        
        
        //Gray layers
        let color : UIColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
        color.setFill()
        UIRectFillUsingBlendMode(CGRectMake(20, 20, 460, 70), kCGBlendModeDarken) //top
        UIRectFillUsingBlendMode(CGRectMake(20, 405, 460, 60), kCGBlendModeDarken) //bottom
        
        //Texts
        var title = texts?["title"] as! String
        title.drawInRect(CGRectMake(60, 45, 400, 400), withAttributes: textFontAttributes)
        drawText.drawInRect(CGRectMake(60, 415, 400, 400), withAttributes: textFontAttributes)
        
        
        //Frame
        var frameImage: UIImage! = UIImage(named: "frame")
        frameImage.drawInRect(CGRectMake(0, 0, 500, 500))
        
        var newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return newImage
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        bgView.image = nil
        mainImageView.image = nil
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func repeatScan(sender: AnyObject) {
        
        let tracker = GAI.sharedInstance().defaultTracker
        var build = GAIDictionaryBuilder.createEventWithCategory("ui_action", action: "button", label: "result_screen_repeat", value: nil).build() as [NSObject : AnyObject]
        tracker.send(build)
        
    }
    
    @IBAction func shareTwitter(sender: AnyObject) {
        
        let tracker = GAI.sharedInstance().defaultTracker
        var build = GAIDictionaryBuilder.createEventWithCategory("ui_action", action: "button", label: "result_screen_share_tw", value: nil).build() as [NSObject : AnyObject]
        tracker.send(build)
        
        
        
        Twitter.sharedInstance().logInWithCompletion { (session: TWTRSession!, error: NSError!) -> Void in
            if session != nil {
                
                // Use the TwitterKit to create a Tweet composer.
                let composer = TWTRComposer()
                
                // Prepare the Tweet with the poem and image.
                composer.setText((self.texts?["sharing"] as! NSDictionary)["after_scan"] as! String)
                var shareimage = self.RBResizeImage( self.mainImageView.image!, targetSize:CGSize(width:200,height:200))
                composer.setImage(shareimage)
                
                // Present the composer to the user.
                composer.showWithCompletion(nil)
            }
        }
        
        
        
        //
        //        if (SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter)){
        //            var controller = SLComposeViewController(forServiceType: SLServiceTypeTwitter);
        //            controller.addImage(mainImageView.image!)
        //            controller.setInitialText((texts?["sharing"] as NSDictionary)["after_scan"] as String)
        //
        //            self.presentViewController(controller, animated: true, completion: nil)
        //        }
        
    }
    @IBAction func shareInstagram(sender: AnyObject) {
        let caption = (texts?["sharing"] as! NSDictionary)["after_scan"] as! NSString
        var shareimage = RBResizeImage( mainImageView.image!, targetSize:CGSize(width:300,height:300))
        instagram.postImage(shareimage, caption: caption , inView: self.view, delegate: self)
    }
    
    func RBResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
        } else {
            newSize = CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    @IBAction func shareFacebook(sender: AnyObject) {
        
        let tracker = GAI.sharedInstance().defaultTracker
        
        
            if (NSLocale.currentLocale().localeIdentifier == "ru_RU"){
                var build = GAIDictionaryBuilder.createEventWithCategory("ui_action", action: "button", label: "result_screen_share_vk", value: nil).build() as [NSObject : AnyObject]
            tracker.send(build)
            
            
            let controller = VKShareDialogController()
            controller.text = (texts?["sharing"] as! NSDictionary)["after_scan"] as! String
            let params : VKImageParameters = VKImageParameters.pngImage()
            var shareimage = RBResizeImage( mainImageView.image!, targetSize:CGSize(width:200,height:200))
            let image = VKUploadImage(image: shareimage, andParams: params)
            let images: [VKUploadImage] = [image]
            controller.uploadImages = images
            controller.completionHandler = { result in
                self.dismissViewControllerAnimated(true, completion: nil)
            }
            self.presentViewController(controller, animated: true, completion:nil)
            
            
        } else {
                var build = GAIDictionaryBuilder.createEventWithCategory("ui_action", action: "button", label: "result_screen_share_fb", value: nil).build() as [NSObject : AnyObject]
            tracker.send(build)
            
            var shareimage = RBResizeImage( mainImageView.image!, targetSize:CGSize(width:300,height:300))
            
            
            //сначала приложение
            if  FBDialogs.canPresentShareDialogWithPhotos()
            {
                FBDialogs.presentShareDialogWithPhotos([shareimage],
                    handler: {
                        call, results, error in
                        if (error != nil){
                            println(error)
                        }
                        else{
                            println("posted to FB")
                        }
                })
            }
            else
            {
                if (SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook)){
                    var controller = SLComposeViewController(forServiceType: SLServiceTypeFacebook);
                    controller.addImage(mainImageView.image!)
                    controller.setInitialText((texts?["sharing"] as! NSDictionary)["after_scan"] as! String)
                    
                    self.presentViewController(controller, animated: true, completion: nil)
                }
                else
                {
                    let link = Settings.getSettings()["iTunes-URL"] as! String
                    let description = (texts?["sharing"] as! NSDictionary)["after_scan"] as! String
                    var fbImage = Settings.getSettings()["sharePic"] as! String
                    let caption = description
                    
                    // Add variables to dictionary
                    var dict = NSMutableDictionary()
                    dict.setValue(title, forKey: "name")
                    dict.setValue(caption, forKey: "caption")
                    dict.setValue(description, forKey: "description")
                    dict.setValue(link, forKey: "link")
                    dict.setValue(fbImage, forKey: "picture")
                    
                    // Present the feed dialog and post the story
                    FBWebDialogs.presentFeedDialogModallyWithSession(nil, parameters: dict as [NSObject : AnyObject],  handler: {
                        call, results, error in
                        if (error != nil){
                            println(error)
                        }
                        else{
                            println("posted to FB")
                        }
                    })
                    
                }
            }
            
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "startSegue"){
            let vc = segue.destinationViewController as! StartViewController
            vc.showAds = true
        }
    }
    
    
    func vkSdkNeedCaptchaEnter(captchaError: VKError!) {
    }
    
    func vkSdkTokenHasExpired(expiredToken: VKAccessToken!) {
        
    }
    
    func vkSdkUserDeniedAccess(authorizationError: VKError!) {
    }
    
    func vkSdkShouldPresentViewController(controller: UIViewController!) {
    }
    
    func vkSdkReceivedNewToken(newToken: VKAccessToken!) {
    }
    
    func vkSdkIsBasicAuthorization() -> Bool {
        return false
    }
    
    
    
    
    
}
